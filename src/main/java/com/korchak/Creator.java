package com.korchak;

import org.apache.logging.log4j.*;

public class Creator {
  private static Logger logger = LogManager.getLogger(Creator.class);

  public static void main(String[] args) {
    logger.trace("TRACE message");
    logger.debug("DEBUG message");
    logger.info("INFO message");
    logger.warn("WARN message");
    logger.error("ERROR message");
    logger.fatal("FATAL message");
  }


}
