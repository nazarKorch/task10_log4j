package com.korchak.SMS;

import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.appender.AbstractAppender;

@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public class AppenderSms extends AbstractAppender {

  protected AppenderSms(String name, Filter filter, Layout<? extends Serializable> layout,
      boolean ignoreException){
    super(name, filter, layout, ignoreException);
  }

  public void append(LogEvent event){
    try {
      Sms.sendSMS(new String(getLayout().toByteArray(event)));

    }catch (Exception e){
      System.out.println(e.getMessage());
    }
  }

  @PluginFactory
  public static AppenderSms createAppender(
      @PluginAttribute("name") String name,
      @PluginElement("Layout") Layout<? extends Serializable> layout,
      @PluginElement("Filter") Filter filter,
      @PluginAttribute("otherAttribute") String otherAttribute){
    if(name == null){
      LOGGER.error("No name");
      return null;
    }
    if(layout == null){
      layout= PatternLayout.createDefaultLayout();
    }
    return new AppenderSms(name, filter, layout, true);
  }

}
